#!/usr/bin/env python
# -*- coding: utf-8 -*-
import sqlite3
import os
import time
import sys
import threading
import random
import subprocess
from threading import Thread
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from utils import Utils

 
def promote(proxy,browser_type,action_on_fail): 
  if browser_type == 'phantomjs':
  
    if sys.platform == 'win32':
      executable_path='webdrivers_win/phantomjs.exe'
    else:
      executable_path='webdrivers_lin/phantomjs' 
    
    useragents = [
      "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:36.0) Gecko/20100101 Firefox/36.0 WebKit",
      "Mozilla/5.0 (Windows; U; Win 9x 4.90; SG; rv:1.9.2.4) Gecko/20101104 Netscape/9.1.0285",
      "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2228.0 Safari/537.36",
      "Opera/9.80 (X11; Linux i686; Ubuntu/14.10) Presto/2.12.388 Version/12.16",
      "Mozilla/5.0 (BlackBerry; U; BlackBerry 9900; en) AppleWebKit/534.11+ (KHTML, like Gecko) Version/7.1.0.346 Mobile Safari/534.11+",
      "Mozilla/5.0 (Linux; U; Android 4.0.3; ko-kr; LG-L160L Build/IML74K) AppleWebkit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30",
      "Mozilla/5.0 (iPad; CPU OS 6_0 like Mac OS X) AppleWebKit/536.26 (KHTML, like Gecko) Version/6.0 Mobile/10A5355d Safari/8536.25",
      "Mozilla/5.0 (iPhone; U; ru; CPU iPhone OS 4_2_1 like Mac OS X; fr) AppleWebKit/533.17.9 (KHTML, like Gecko) Version/5.0.2 Mobile/8C148a Safari/6533.18.5",
      "Mozilla/5.0 (X11; Linux i586; rv:31.0) Gecko/20100101 Firefox/31.0",
      "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36",
      "Mozilla/5.0 (compatible; Baiduspider/2.0; +http://www.baidu.com/search/spider.html)",
      "Mozilla/5.0 (iPhone; CPU iPhone OS 7_0 like Mac OS X) AppleWebKit/537.51.1 (KHTML, like Gecko) Version/7.0 Mobile/11A465 Safari/9537.53 (compatible; bingbot/2.0; http://www.bing.com/bingbot.htm)",
      "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.0; Trident/5.0; Trident/5.0)",
      "Mozilla/5.0 (X11; U; Linux x86_64; de; rv:1.9.2.8) Gecko/20100723 Ubuntu/10.04 (lucid) Firefox/3.6.8",
      "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; FSL 7.0.6.01001)",
      "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; FSL 7.0.5.01003)"
    ]

    dcap = dict(DesiredCapabilities.PHANTOMJS)
    dcap["phantomjs.page.settings.userAgent"] = (random.choice(useragents))
    dcap["phantomjs.page.customHeaders.Accept-Language"] = 'en-US'
    
    service_args = ['--ignore-ssl-errors=true','--ssl-protocol=any']
    service_args.append('--proxy=%s' % proxy)
    service_args.append('--proxy-type=https')  
    
    browser = webdriver.PhantomJS(executable_path=executable_path,desired_capabilities=dcap,service_args=service_args)
  
  elif browser_type == 'chrome':
  
    if sys.platform == 'win32':
      executable_path='webdrivers_win/chromedriver.exe'
    else:
      executable_path='webdrivers_lin/chromedriver' 
      
    prefs = {"profile.default_content_setting_values.notifications" : 2}
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--proxy-server=%s' % proxy)
    chrome_options.add_experimental_option("prefs",prefs)
  
    browser = webdriver.Chrome(executable_path=executable_path,chrome_options=chrome_options)

  utils = Utils()
  
  #utils.debug_switch = True
  browser.set_page_load_timeout(30)
  browser.delete_all_cookies()
  browser.refresh()
  
  try:
    # Google search section
    browser.get("https://www.google.com/search?q=" + "givemeproxy.com" + "&start=0")
    result = utils.get_element_by_xpath(browser,20,'.//h3//*[contains(text(),"Free SSL Proxy list - Serf the web anonymously")]','google result line')
    utils.element_click(browser,result,"google result")
    if result:
      print "Google-Status : Success"
    else:
      print "Google-Status : Failed"
      action_on_fail(proxy)
  except TimeoutException as e:
    print "Google-Status : No Link"
    action_on_fail(proxy)
  except Exception as e:
    print "Google-Status : Error - %s" % (e)
    action_on_fail(proxy)
  
  try:
    # Givemeproxy search section
    browser.get("http://givemeproxy.com")
    pagin = utils.get_elements_by_css(browser,25,'.dataTables_paginate a.next','next page')
    utils.element_click(browser,pagin[0],"pagin click")
    if pagin:
      print "GMP-Status : Success"
    else:
      print "GMP-Status : Failed"
  except TimeoutException as e:
    print "GMP-Status : No Link"
  except Exception as e:
    print "GMP-Status : Error - %s" % (e)

  

  browser.quit() 

def proxy_delete(proxy):
  try:
    conn = sqlite3.connect('dataStorage.db')
    c = conn.cursor()
    c.execute("DELETE FROM proxy_list WHERE proxy = ?",(proxy))
    conn.commit()
    print "successfully delete Proxy"
  except Exception as e:
    print "could not delete proxy" 

while True:
  try:           
    conn = sqlite3.connect('dataStorage.db')
    c = conn.cursor()
    c.execute("SELECT proxy FROM proxy_list")
    proxy_list = c.fetchall()
    threads = []
    browser_type = 'phantomjs'
    if proxy_list:
      for row in range(len(proxy_list)):  
        try:
          print "proxy : %s" % (proxy_list[row])
          t = Thread(target=promote, args=(proxy_list[row],browser_type,proxy_delete, ))
          t.start()
          threads.append(t)
          
        except Exception as e:
          print e
          print "Error: unable to start thread"
          
        if (row % 15 == 0 and row != 0) or (len(proxy_list)-1-row == 0):
          print "Joining threads"
          for t in threads:
            t.join()
    else:
      commands = []
      commands.append("python proxy_tester.py")
      processes = [subprocess.Popen(cmd, shell=True) for cmd in commands]
      for p in processes: p.wait()
          
  except Exception as e:
    print e
  

