#!/usr/bin/env python
# -*- coding: utf-8 -*-

import urllib2, socket
import subprocess
import pprint
import time
import sys
import os
import sqlite3
import pytesseract
import cStringIO
import signal
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from threading import Thread
from PIL import Image
from utils import Utils

class Proxy_tester:

  # constructor
  def __init__(self):

    self.utils = Utils()
    self.proxy_list = []

    print "Starting browser"
    dcap = dict(DesiredCapabilities.PHANTOMJS)
    dcap["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:36.0) Gecko/20100101 Firefox/36.0 WebKit")
    service_args = ['--ignore-ssl-errors=true','--ssl-protocol=any']
    
    if sys.platform == 'win32':
      self.browser = webdriver.PhantomJS(executable_path='webdrivers_win/phantomjs.exe',desired_capabilities=dcap,service_args=service_args,service_log_path=os.path.devnull)
    else:
      '''      
      prefs = {"profile.default_content_setting_values.notifications" : 2}
      chrome_options = webdriver.ChromeOptions()
      chrome_options.add_experimental_option("prefs",prefs)
      self.browser = webdriver.Chrome(executable_path=executable_path,chrome_options=chrome_options)
      '''
      self.browser = webdriver.PhantomJS(executable_path='webdrivers_lin/phantomjs',desired_capabilities=dcap,service_args=service_args,service_log_path=os.path.devnull)  
      executable_path='webdrivers_lin/chromedriver' 
      self.browser.set_page_load_timeout(60)
      self.browser.delete_all_cookies()
      self.browser.refresh() 

  # destructor
  def __del__(self):
    print "Closing browser"
    #self.browser.service.process.send_signal(signal.SIGTERM)
    self.browser.quit()
    
  def get_torvpn_server_proxies(self):
    try:
      browser = self.browser
      utils = self.utils
      browser.get("https://www.torvpn.com/en/proxy-list")
      
      table = utils.get_elements_by_css(browser,60,'table.table-striped tbody tr')
      for row in range(len(table)):
        ip_addr   = utils.get_elements_by_css(table[row],10,'td:nth-child(2) img')
        port_addr = utils.get_element_text_by_css(table[row],10,'td:nth-child(3)')
        country   = utils.get_element_text_by_css(table[row],10,'td:nth-child(4) abbr')
        if ip_addr:
          img_src = ip_addr[0].get_attribute("src")
          file = cStringIO.StringIO(urllib2.urlopen(img_src).read())
          image = Image.open(file)
          ip_addr = pytesseract.image_to_string(image)
          self.proxy_list.append(ip_addr+':'+port_addr) 
          print ip_addr+':'+port_addr
      
    except Exception as e:
      print e 

  def get_sslproxies_server_proxies(self):
    try:
      browser = self.browser
      utils = self.utils
      browser.get("https://www.sslproxies.org")
      
      while True:
        table = utils.get_elements_by_css(browser,60,'table.dataTable tbody tr')
        for row in range(len(table)):
          ip_addr   = utils.get_element_text_by_css(table[row],10,'td:nth-child(1)')
          port_addr = utils.get_element_text_by_css(table[row],10,'td:nth-child(2)')
          country   = utils.get_element_text_by_css(table[row],10,'td:nth-child(4)')
          self.proxy_list.append(ip_addr+':'+port_addr) 
          print ip_addr+':'+port_addr
        next = utils.get_elements_by_css(browser,10,'ul.pagination li.active + li:not(.disabled) a')
        if next:
          utils.element_click(browser,next[0],"click next button on sslproxies_server_proxies")
        else:
          break
    except Exception as e:
      print e 
    
  def get_hidemy_server_proxies(self):
    try:
      browser = self.browser
      utils = self.utils
      browser.get("https://hidemy.name/en/proxy-list/")
      
      while True:
        table = utils.get_elements_by_css(browser,60,'.proxy__t tbody tr')
        for row in range(len(table)):
          ip_addr   = utils.get_element_text_by_css(table[row],10,'td:nth-child(1)')
          port_addr = utils.get_element_text_by_css(table[row],10,'td:nth-child(2)')
          country   = utils.get_element_text_by_css(table[row],10,'td:nth-child(3)')
          self.proxy_list.append(ip_addr+':'+port_addr) 
          print ip_addr+':'+port_addr
        next = utils.get_elements_by_css(browser,10,'div.proxy__pagination ul li.is-active + li a')
        if next:
          utils.element_click(browser,next[0],"click next button on hidemy_server_proxies")
        else:
          break
    except Exception as e:
      print e 
    
  def get_proxylist_server_proxies(self):
    try:
      browser = self.browser
      utils = self.utils
      browser.get("https://proxy-list.org/english/index.php")
      
      while True:
        table = utils.get_elements_by_css(browser,60,'div.table ul')
        for row in range(len(table)):
          proxy     = (utils.get_element_text_by_css(table[row],10,'li.proxy')).split(':')
          ip_addr   = proxy[0]
          port_addr = proxy[1]
          country   = utils.get_element_text_by_css(table[row],10,'li.country-city .country .name')
          self.proxy_list.append(ip_addr+':'+port_addr) 
          print ip_addr+':'+port_addr
        next = utils.get_elements_by_css(browser,10,'div.table-menu a.active + a')
        if next:
          utils.element_click(browser,next[0],"click next button on proxylist_server_proxies")
        else:
          break
    except Exception as e:
      print e 
    

  def insert_proxy_to_database(self,proxy):
    try:
      conn = sqlite3.connect('dataStorage.db')
      c = conn.cursor()
      c.execute("INSERT INTO proxy_list VALUES (?,?,?,?)",(None,proxy,None,None))
      conn.commit()
      c.close()
      conn.close()
      print "successfully add Proxy"
    except sqlite3.IntegrityError:
      print "could not add Proxy twice" 
    except Exception as e:
      print "failed to add Proxy : %s" % (e)
        
  def check_server_proxy(self):
    
    proxy_list = self.proxy_list
    conn = sqlite3.connect('dataStorage.db')
    c = conn.cursor()
    c.execute("CREATE TABLE IF NOT EXISTS proxy_list(id INTEGER PRIMARY KEY,proxy varchar unique,username varchar,password varchar)")
    c.close()
    conn.close()
    
    self.get_proxylist_server_proxies()
    self.get_sslproxies_server_proxies()
    self.get_hidemy_server_proxies()
    #self.get_torvpn_server_proxies()
    threads = []

    for row in range(len(proxy_list)):
      try:
        print "proxy : %s" % (proxy_list[row])
        t = Thread(target=self.is_good_proxy, args=(proxy_list[row], ))
        t.start()
        threads.append(t)
        
      except Exception as e:
        print e
        print "Error: unable to start thread"
        
      if (row % 20 == 0 and row != 0) or (len(proxy_list)-1-row == 0):
        print "Joining threads"
        for t in threads:
          t.join()
          
  def is_good_proxy(self,proxy):    
      try:        
          proxy_handler = urllib2.ProxyHandler({'http': proxy})        
          opener = urllib2.build_opener(proxy_handler)
          opener.addheaders = [('User-agent', 'Mozilla/5.0')]
          urllib2.install_opener(opener)        
          req=urllib2.Request('http://www.google.com')
          sock=urllib2.urlopen(req, timeout = 2)
      except urllib2.HTTPError, e:        
          return False
      except Exception, detail:
          return False
      self.insert_proxy_to_database(proxy)
      print "Proxy %s is good!" % (proxy) 

if __name__ == "__main__":      
  proxy_tester = Proxy_tester()
  proxy_tester.check_server_proxy()
